//VALOR PROMEDIO
function ValorPromedio(arreglo = [1,2,3,5,6,7,8,9,10,12,12,13,14,5,6,7,8,9,1,9]){
    let array = document.getElementById('arrayValorPromedio');
    let valor = document.getElementById('valorPromedio');
    array.innerHTML += arreglo;
    var prom, sum= 0;
    for (let i = 0; i<20; i++) {
        sum = sum + arreglo[i];  
    }
    prom = sum/20;
    valor.innerHTML += prom; 
}

//NUMEROS PARES
function NumerosPares(arreglo = [1,2,3,5,6,7,8,9,10,12,12,13,14,5,6,7,8,9,1,9]){
    let array = document.getElementById('arrayNumerosPares');
    let pares = document.getElementById('numerosPares');
    var contP = 0;
    array.innerHTML += arreglo;
    for (let i = 0; i<20; i++) {
        if (arreglo[i] %2 == 0){
            contP = contP + 1;
        }  
    }
    pares.innerHTML += contP;
}

//Ordenar de mayor a menor
function MayorMenor(arreglo = [1,2,3,5,6,7,8,9,10,12,12,13,14,5,6,7,8,9,1,9]){
    let array = document.getElementById('arrayNumMayMen');
    let orden = document.getElementById('numMayMen');
    array.innerHTML += arreglo; 
    orden.innerHTML += arreglo.sort(function(a, b){return b - a}); 
}